//TODO add refresh  button

let UID;
let API_TOKEN;
let signedIn = false;

let todoList;
let todos;

let dailies;
let dailiesList;

let todaysDate = new Date();
let dayOfWeek = todaysDate.getDay();
let date = todaysDate.getDate();

var converter = new showdown.Converter({emoji: true});

let month = todaysDate.getMonth();
let monthText = document.getElementById('month');
switch(month)
{
  case 0:
  monthText.firstChild.textContent = "January";
  break;
  case 1:
  monthText.firstChild.textContent = "February";
  break;
  case 2:
  monthText.firstChild.textContent = "March";
  break;
  case 3:
  monthText.firstChild.textContent = "April";
  break;
  case 4:
  monthText.firstChild.textContent = "May";
  break;
  case 5:
  monthText.firstChild.textContent = "June";
  break;
  case 6:
  monthText.firstChild.textContent = "July";
  break;
  case 7:
  monthText.firstChild.textContent = "August";
  break;
  case 8:
  monthText.firstChild.textContent = "September";
  break;
  case 9:
  monthText.firstChild.textContent = "October";
  break;
  case 10:
  monthText.firstChild.textContent = "November";
  break;
  case 11:
  monthText.firstChild.textContent = "December";
  break;

  default:
  monthText.firstChild.textContent = "Invalid Month";

}


var weekDayofFirst = dayOfWeek;

for (var i = date - 1; i > 0; i--)
{
  weekDayofFirst--;
  if (weekDayofFirst < 0)
  {
    weekDayofFirst = 6;
  }
}

function daysInMonth(month, year)
{
  return new Date(year, month, 0).getDate();
}

const URL = "https://habitica.com/api/v3/tasks/user";
const params = {
  'type': "daily"
}

var tables = document.getElementsByClassName("table"); //just create a damn table in code instead lol
var table = document.createElement("TABLE");
document.body.appendChild(table);
table.classList.add('table'); //TODO fix this mess below lmao hopefully no one reads this.
table.innerHTML = "<thead><tr class=\"weekdays\"><th scope=\"col\">Sunday</th><th scope=\"col\">Monday</th><th scope=\"col\">Tuesday</th><th scope=\"col\">Wednesday</th><th scope=\"col\">Thursday</th><th scope=\"col\">Friday</th><th scope=\"col\">Saturday</th></tr></thead><tbody><tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr></tbody>"
//table.classList.add('table-light'); //TODO Fix table Colors
//table.classList.add('table-hover');
table.classList.add('table-bordered');

function createCalendar()
{
  var xTotal = 0;
  for (var col = 1; col < 6; col++)
  {
    for (var x = 0; x < 7; x++)
    {
      if (col == 1)
      {
        if (x >= weekDayofFirst)
        {
          xTotal++;
          var dateNode = document.createElement('div');
          dateNode.innerHTML = (x - weekDayofFirst + 1) * col;
          table.rows[col].childNodes[x].innerHTML = "";
          table.rows[col].childNodes[x].appendChild(dateNode);
        }
      }
      else
      {
        xTotal++;
        if (xTotal - 1 > daysInMonth(todaysDate.getMonth(), todaysDate.getYear()))
        {}
        else
        {
          var dateNode = document.createElement('div');
          dateNode.innerHTML = xTotal;
          table.rows[col].childNodes[x].innerHTML = "";
          table.rows[col].childNodes[x].appendChild(dateNode);
        }
      }
    }
  }
}

createCalendar();

var endOfWeek = 7 - weekDayofFirst;

function fillTableTodos()
{
  var date;
  var todoDate;
  var todoWeekDay;
  for (var j = 0; j < todoList.length; j++)
  {
    date = new Date(todoList[j].date);
    todoDate = date.getDate();
    todoWeekDay = date.getDay();
    var todoMonth = date.getMonth();
    if (todoMonth == todaysDate.getMonth())
    {

      for (var row = 1; row < table.rows.length; row++)
      {

        for (var col = 0; col < table.rows[row].childNodes.length; col++)
        {

          var dateDIV = document.createElement('div');
          dateDIV.innerHTML = todoDate;
          if (table.rows[row].childNodes[col].firstChild == null)
          {}
          else
          {
            if (table.rows[row].childNodes[col].firstChild.innerHTML == dateDIV.innerHTML)
            {
              var informationDiv = document.createElement('li');
              informationDiv.innerHTML = converter.makeHtml(todoList[j].text);
              informationDiv.style.color = "#24cc8f";
              // informationDiv.style.fontWeight = 'bold';
              if (todaysDate.getDate() == todoDate)
              {
                informationDiv.style.color = "red";
              }
              //table.rows[row].childNodes[col].innerHTML += todoList[j].text
              table.rows[row].childNodes[col].appendChild(informationDiv);
            }
          }
        }
      }
    }
  }
}

function fillTableDailies()
{
  dailiesList.forEach(function(currDaily)
  {
    if(currDaily.everyX < 1)
    {
      return;
    }
    if (currDaily.isDue)
    {
      var count = 0;
      Array.from(table.rows).forEach(function(row)
      {
        if (count > 0)
        {
          row.childNodes.forEach(function(col)
          {
            if (col.firstChild == null)
            {}
            else
            {
              var dateDIV = document.createElement('div');
              let todaysDate = new Date();
              dateDIV.innerHTML = todaysDate.getDate();
              if (col.firstChild.innerHTML == dateDIV.innerHTML)
              {
                var informationDiv = document.createElement('li');
                informationDiv.innerHTML = converter.makeHtml(currDaily.text);
                informationDiv.style.color = "blue";

                col.appendChild(informationDiv);
              }
            }
          });
        }
        count++;
      });
    }

    var nextDue = currDaily.nextDue;
    nextDue.forEach(function(day)
    {
      var date = new Date(day);
      var month = date.getMonth();


      if (month == todaysDate.getMonth())
      {
        if(date.getDate() == todaysDate.getDate())
        {
          return;
        }
        var count = 0;
        Array.from(table.rows).forEach(function(row)
        {
          if (count > 0)
          {
            row.childNodes.forEach(function(col)
            {
              if (col.firstChild == null)
              {}
              else
              {
                var dateDIV = document.createElement('div');
                dateDIV.innerHTML = date.getDate();
                if (col.firstChild.innerHTML == dateDIV.innerHTML)
                {
                  var informationDiv = document.createElement('li');
                  informationDiv.innerHTML = converter.makeHtml(currDaily.text);

                  col.appendChild(informationDiv);
                }
              }
            });
          }
          count++;
        });
      }
    });
  });
}

function userSubmitted(form)
{
  if (!form[0].value)
  {
    console.log('No User Name');
    alert('No UID Entered');
    return;
  }
  if (!form[1].value)
  {
    console.log('No API Token');
    alert('No API Token Entered');
    return;
  }
  //Clear Calendar
  createCalendar();
  UID = form[0].value;
  API_TOKEN = form[1].value;

  signedIn = true;

  todos = axios.create(
  {
    baseURL: 'https://habitica.com/api/v3/',
    timeout: 10000,
    headers:
    {
      'x-api-user': UID,
      'x-api-key': API_TOKEN
    },
    params:
    {
      type: "todos"
    }
  });

  todos.get(URL)
    .then(function(response)
    {
      todoList = response.data.data;
    })
    .catch(function(error)
    {
      console.log("We have an Error!\n" + error);
      if (error.response)
      {
        console.log(error.response.data);
        console.log(error.response.status);
        console.log(error.response.headers);
      }
      else if (error.request)
      {
        console.log(error.request);
      }
      else
      {
        console.log('Error', error.message);
      }
      console.log(error.config);
    })
    .then(function()
    {
      console.log("Todos Get Done!");
      fillTableTodos();
    });



  dailies = axios.create(
  {
    baseURL: 'https://habitica.com/api/v3/',
    timeout: 10000,
    headers:
    {
      'x-api-user': UID,
      'x-api-key': API_TOKEN
    },
    params:
    {
      type: "dailys"
    }
  });

  dailies.get(URL)
    .then(function(response)
    {
      dailiesList = response.data.data;
    })
    .catch(function(error)
    {
      console.log("We have an Error!\n" + error);
      if (error.response)
      {
        console.log(error.response.data);
        console.log(error.response.status);
        console.log(error.response.headers);
      }
      else if (error.request)
      {
        console.log(error.request);
      }
      else
      {
        console.log('Error', error.message);
      }
      console.log(error.config);
    })
    .then(function()
    {
      console.log("Dailies Get Done!");
      fillTableDailies();
    });

  form.reset();

}


function userRefresh()
{
  if (signedIn)
  {
    createCalendar();
    todos.get(URL)
      .then(function(response)
      {
        todoList = response.data.data;
      })
      .catch(function(error)
      {
        console.log("We have an Error!\n" + error);
        if (error.response)
        {
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
        }
        else if (error.request)
        {
          console.log(error.request);
        }
        else
        {
          console.log('Error', error.message);
        }
        console.log(error.config);
      })
      .then(function()
      {
        console.log("Todos Get Done!");
        fillTableTodos();
      });

    dailies.get(URL)
      .then(function(response)
      {
        dailiesList = response.data.data;
      })
      .catch(function(error)
      {
        console.log("We have an Error!\n" + error);
        if (error.response)
        {
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
        }
        else if (error.request)
        {
          console.log(error.request);
        }
        else
        {
          console.log('Error', error.message);
        }
        console.log(error.config);
      })
      .then(function()
      {
        console.log("Dailies Get Done!");
        fillTableDailies();
      });
  }
  else {
    alert('Not signed in!');
  }
}
